<?php

use Illuminate\Database\Seeder;

use Illuminate\Support\Facades\DB;

use App\User;
use App\Entry;
use App\Part;
use App\Card;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /* DB::table('users')->insert([
            'name' => "Maciej Kułak",
            'email' => "mkulak0@gmail.com",
            'password' => bcrypt("zaq1@WSX")
        ]); */

        $user_id = User::create([
            'name' => "Maciej Kułak",
            'email' => "mkulak0@gmail.com",
            'password' => bcrypt("zaq1@WSX")
        ])->id;

        $entry_id = Entry::create([
            'user_id' => $user_id,
            'observation' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',
            'request' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',
            'created_at' => date("Y-m-d H:i:s")
        ])->id;

        $card_id = Card::create([
            'slug_name' => 'trap_card',
            'type' => 'feeling'
        ])->id;

        $part_id = Part::create([
            'card_id' => $card_id,
            'slug_name' => 'happiness_part',
            'coordinate' => '{x: 123, y:312, r:5}'
        ])->id;

        DB::table('entry_part')->insert([
            'entry_id' => $entry_id,
            'part_id' => $part_id
        ]);

        $part_id = Part::create([
            'card_id' => $card_id,
            'slug_name' => 'affection_part',
            'coordinate' => '{x: 123, y:312, r:5}'
        ])->id;

        DB::table('entry_part')->insert([
            'entry_id' => $entry_id,
            'part_id' => $part_id
        ]);
    }
}
