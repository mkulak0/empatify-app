<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEntryPart extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('entry_part', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('entry_id')->unsigned()->nullable();;
            $table->bigInteger('part_id')->unsigned()->nullable();;

            $table->foreign('entry_id')->references('id')->on('entries');
            $table->foreign('part_id')->references('id')->on('parts');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('parts', function (Blueprint $table) {
            $table->dropForeign(['entry_id']);
            $table->dropForeign(['part_id']);
        });
        Schema::dropIfExists('entry_part');
    }
}
