<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/* Registration and login */

Route::get('/user', function (Request $request){
    return $request->user();
})->middleware('auth:api');

/* Route::get('/auth', 'Auth\LoginController@authenticate'); */

Route::post('/login', 'Auth\LoginController@login');

Route::post('/register', 'Auth\RegisterController@register');

/* Logged users */

Route::middleware(['auth:api'])->group(function () {
    Route::get('/entry', 'EntryController@index');
    Route::get('/entry/{id}', 'EntryController@show');
    Route::post('/entry', 'EntryController@store');
    Route::put('/entry/{id}', 'EntryController@update');
    Route::delete('/entry/{id}', 'EntryController@destroy');

    Route::get('/card', 'CardController@index');
});
