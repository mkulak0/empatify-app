import React from 'react';
import {Link} from 'react-router-dom';

import Aux from '../Aux/Aux';

import classes from './Layout.css';

const Layout = (props) => (
    <Aux>
        <p>EmpatifyApp</p>
        <div className={classes.Links}>
            <Link to='/'>Home</Link>
            <Link to='/login'>Login</Link>
        </div>
        <div className={classes.DebugLinks}>
            <button onClick={props.auth}>Auth!</button>
        </div>
        {props.children}
    </Aux>
);

export default Layout;