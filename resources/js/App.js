import React, {Component} from 'react';
import {BrowserRouter} from 'react-router-dom';

import {Route, Switch, Redirect} from 'react-router-dom';

import Layout from './hoc/Layout/Layout';
import Aux from './hoc/Aux/Aux';

import Login from './containers/Login/Login';


class App extends Component {

    state = {
        auth: false
    }

    authHandler = () => {
        this.setState({
            auth: true
        });
    }

    render() {
        let componentsForAuth = null;
        if(this.state.auth){
            componentsForAuth = (
                <Aux>
                    <h1>You are authenthicated!</h1>
                </Aux>
            );
        }
        return (
            <BrowserRouter>
                <Layout auth={this.authHandler}>
                    <Switch>
                        <Route 
                            path='/login'
                            component={Login}
                        />
                        {componentsForAuth}
                    </Switch>
                </Layout>
            </BrowserRouter>
        );
    }
}

export default App;