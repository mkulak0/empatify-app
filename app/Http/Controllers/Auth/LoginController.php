<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;

use App\User;
use App\Http\Requests\LoginUser;


class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/app';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    /* Nie jestem pewny czy to do końca jest potrzebne */
    /* public function authenticate(Request $request)
    {
        $credentials = $request->only('email', 'password');
        
        if (Auth::attempt($credentials)) {
            // Authentication passed...
            return redirect()->intended('dashboard');
        }
    } */

    
    public function login(LoginUser $request){

         $request = $request->validated();

         if(User::where('email', $request['email'])->exists()){
            $user = User::where('email', $request['email'])->first();
            $auth = Hash::check($request['password'], $user->password);
            
            if($user && $auth){

               $user->rollApiKey();
         
               return response(array(
                  'user' => $user,
                  'message' => 'Authorization Successful!',
               ));
            }
         }
         
         return response(array(
            'message' => 'Unauthorized, check your credentials.',
         ), 401);
    }
}
