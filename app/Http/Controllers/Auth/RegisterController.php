<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

use Illuminate\Support\Str;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Response;
use App\Http\Requests\RegisterUser;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    //use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/app';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('guest');
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function register(RegisterUser $request)
    {
        /* Get Validated data */
        $data = $request->validated();

        $user = new User;
        $user->name = $data['name'];
        $user->email = $data['email'];
        $user->password = Hash::make($data['password']);
        $user->api_token = Str::random(60);

        $user->save();

        return $user;
    }

    protected function register_deprecated(RegisterUser $request)
    {
        /* Get Validated data */
        $data = $request->validated();

        /* Getting row with registration code */
        $db_result = DB::table('registration_code')->where([
            ['code', $data['code']],
            ['active', true],
        ]);

        /* If registration code exists create user and assign code_id */
        if($db_result->count() == 1){

            
            $user = new User;
            $user->name = $data['name'];
            $user->email = $data['email'];
            $user->password = Hash::make($data['password']);
            $user->api_token = Str::random(60);
            $user->code_id = $db_result->value('id');
            
            $db_result->update(['active' => false]);

            $user->save();

            return $user;
        } else {
            /* Or display error */
            return response()->json([
                'message' => 'The given data was invalid.',
                'errors' => [
                    'code' => 'Ten kod autoryzacyjny jest już w użyciu.'
                ], 422
            ]);
        }
    }
}
