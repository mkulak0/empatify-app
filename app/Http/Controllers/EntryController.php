<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use Illuminate\Support\Facades\DB;

use App\Card;
use App\Entry;

class EntryController extends Controller
{
    /* Generate additional attributes to entry */
    private function generateEntry($entry){
        /* Adding Feelings with cards*/
        $entry['feelings'] = $entry->feelings()->get();
        foreach($entry->feelings as $feeling){
            $feeling['card'] = Card::find($feeling->card_id)->first();
        }
        /* Adding Needs with cards */
        $entry['needs'] = $entry->needs()->get();        
        foreach($entry->needs as $need){
            $need['card'] = Card::find($need->card_id)->first();
        }
        return $entry;
    }

    /* Attaching feelings/needs($hook) to entry */
    private function attachToEntry($hook, $body, $table_name){
        foreach($body[$table_name] as $object){
            $id = DB::table($table_name)
                ->where('slug_name', $object)
                ->first()->id;
            $hook->attach($id);
        }
    }

    public function index(Request $request){
        $entries = Auth::user()->entries()->get();

        foreach($entries as $entry){
            $entry = $this->generateEntry($entry);
        }

        return $entries;
    }

    public function show(Request $request, $entry_id){
        $user_id = Auth::user()->id;

        $entry = Entry::where([
            'user_id' => $user_id,
            'id' => $entry_id
        ])->first();

        if(gettype($entry) === 'NULL'){
            return response()->json([
                'message' => 'Brak takiego wpisu.'
            ]);
        }
        
        $entry = $this->generateEntry($entry);
        return $entry;
    }

    public function store(Request $request){
        $body_entry = $request->all();
        $user =  Auth::User();

        /* Dodawanie wpisu */
        $entry = $user->entries()->create([
            'user_id' => $user->id,
            'created_at' => $body_entry['created_at'],
            "observation" => $body_entry['observation'],
            "request" => $body_entry['request']
        ]);

        /* Dodawanie uczuć do wpisu */
        $feelings = $entry->feelings();

        $this->attachToEntry($feelings, $body_entry, 'feelings');

        /* Dodawanie potrzeb do wpisu */
        $needs = $entry->needs();

        $this->attachToEntry($needs, $body_entry, 'needs');

        return [
            "message" => "Dodano pomyślnie!"
        ];
    }

    public function update(Request $request, $id){

        $update_entry = $request->all();

        /* Update entry */
        $entry = Entry::find($id);

        /* Checking if entry exists */
        if(gettype($entry) === 'NULL'){
            return response()->json([
                'message' => 'Brak takiego wpisu.'
            ]);
        }

        $entry->observation = $update_entry['observation'];
        $entry->request = $update_entry['request'];

        /* Detaching */
        $entry->feelings()->detach();
        $entry->needs()->detach();

        /* Attaching */
        $this->attachToEntry($entry->feelings(), $update_entry, 'feelings');
        $this->attachToEntry($entry->needs(), $update_entry, 'needs');

        $entry->save();

        return $entry;
    }

    public function destroy(Request $request, $id){
        /* Getting entry */
        $entry = Entry::find($id);

        /* Checking if entry exists */
        if(gettype($entry) === 'NULL'){
            return response()->json([
                'message' => 'Brak takiego wpisu.'
            ]);
        }

        
    }
}
