<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Card;

class CardController extends Controller
{
    public function index(Request $request){
        return Card::all();
    }
}
