<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class LoginUser extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' => 'required|email|string|max:255',
            'password' => 'required|string'
        ];
    }

    public function messages(){
        return [
            'email.required' => 'Wpisz swój email.',
            'password.required' => 'Wpisz swoje hasło aby chronić swoje konto.'
        ];
    }
}
