<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RegisterUser extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string'], // 'confirmed' 'min:8'
            //'code' => ['required', 'string'],
        ];
    }

    public function messages(){
        return [
            'name.required' => 'Wpisz swoją nazwę użytkownika.',
            'email.required' => 'Wpisz swój email.',
            'email.unique' => 'Twój email jest już w użyciu.',
            'password.required' => 'Wpisz swoje hasło aby chronić swoje konto.',
            //'code.required' => 'Kod autoryzacyjny jest wymagany.'
        ];
    }
}
