<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Entry extends Model
{
    /* Deletes Timestamps */
    public $timestamps = false;
    
    protected $fillable = [
        'user_id', 'observation', 'created_at', 'request'
    ];

    public function parts(){
        return $this->belongsToMany('App\Part');
    }
}
