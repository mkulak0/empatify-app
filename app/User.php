<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Model;

//class User extends Authenticatable
class User extends Model
{
    //use Notifiable;

    /**
     * The attributes that are mass assignable.
     */
    protected $fillable = [
        'name', 'email', 'password', 'api_token',
    ];

    /**
     * The attributes that should be hidden for arrays.
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $guarded = [];

    /**
     * The attributes that should be cast to native types.
     */
    /* protected $casts = [
        'email_verified_at' => 'datetime',
    ]; */

    /* Deletes Timestamps */
    public $timestamps = false;

   /*  Roll API Key*/
    public function rollApiKey(){
        do{

            $this->api_token = Str::random(60);

        }while($this->where('api_token', $this->api_token)->exists());

        $this->save();
    }

    /* ================================================================== */

    public function entries(){
        return $this->hasMany('App\Entry');
    }
}
