<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Part extends Model
{
    /* Deletes Timestamps */
    public $timestamps = false;

    protected $fillable = [
        'card_id', 'slug_name', 'coordinate'
    ];

    /* Castowanie jsona do tablicy */
    protected $casts = [
        'coordinate' => 'array'
    ];

     /* Usuwanie atrybutu pivot */
     protected $hidden = [
        'pivot'
    ];

    public function card(){
        return $this->hasOne('App\Card');
    }
}
