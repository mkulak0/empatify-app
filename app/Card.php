<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Card extends Model
{
    /* Deletes Timestamps */
    public $timestamps = false;
    
    protected $fillable = [
        'name', 'category_name', 'slug_name'
    ];

    public function feelings(){
        return $this->hasMany('App\Feeling');
    }

    public function needs(){
        return $this->hasMany('App\Need');
    }
}
